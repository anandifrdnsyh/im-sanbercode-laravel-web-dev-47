<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Template
// Route::get('/master', function(){
//     return view('layouts.master');
// });

Route::get('/', [HomeController::class , 'index'])->name("home");
Route::get('/register', [AuthController::class , 'index'])->name("register");
Route::post('/welcome', [AuthController::class , 'welcome'])->name("welcome");

Route::get('/table', function(){
    return view('halaman.table');
})->name("table");

Route::get('/data-table', function(){
    return view('halaman.data-table');
})->name("data-table");


// CRUD
Route::get('/cast', [CastController::class, 'index'])->name("cast");
Route::get('/cast/{id}', [CastController::class, 'show'])->name("castShow");
Route::get('/cast/create', [CastController::class, 'create'])->name("castCreate");
Route::post('/cast', [CastController::class, 'store'])->name("castStore");
Route::get('/cast/{id}/edit', [CastController::class, 'edit'])->name("castEdit");
Route::put('/cast/{id}', [CastController::class, 'update'])->name("castUpdate");
Route::delete('/cast/{id}', [CastController::class, 'destroy'])->name("castDestroy");

