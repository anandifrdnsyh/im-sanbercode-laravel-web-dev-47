<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function index(){
        $cast = DB::table('cast')->get();
        // dd($cast[0]->nama);
        // dd($cast[1]->nama);

        return view('cast.tampil',['cast' => $cast ]);
    }
    public function create(){

        return view('cast.tambah');
    }
    public function store(Request $request){

        $request->validate([
            'name' => 'required',
            'umur' => 'required|numeric',
            'bio' => 'required|min:5',
        ]);

        DB::table('cast')->insert([
        'nama' => $request->name,
        'umur' => $request->umur,
        'bio' => $request->bio
        ]);
        return redirect()->route('cast');
    }
      public function show($id){
        $cast = DB::table('cast')->find($id);
        // dd($cast);
        return view('cast.detail',['cast' => $cast ]);
    }
       public function edit($id){
        $cast = DB::table('cast')->find($id);
        // dd($cast);
        return view('cast.edit',['cast' => $cast ]);
    }
     public function update(Request $request, $id){
          $request->validate([
            'name' => 'required',
            'umur' => 'required|numeric',
            'bio' => 'required|min:5',
        ]);
        $cast = DB::table('cast')
              ->where('id', $id)
              ->update(
                [
                'nama' => $request->name,
                'umur' => $request->umur,
                'bio' => $request->bio
                ]
            );

        return redirect()->route('cast');
    }
        public function destroy($id){
        DB::table('cast')->where('id', '=', $id)->delete();
        // dd($cast);
        return redirect()->route('cast');
    }
}
