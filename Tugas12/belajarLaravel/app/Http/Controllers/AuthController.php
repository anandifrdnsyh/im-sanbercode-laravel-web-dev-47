<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index(){
        return view('halaman.register');
    }

    public function welcome(Request $request) {
        // dd($request->fname);
        // dd($request->lname);
        $namaDepan = $request->fname;
        $namaBelakang = $request->lname;

        return view('halaman.home', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
}
