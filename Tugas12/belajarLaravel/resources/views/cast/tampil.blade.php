@extends('layouts.master')

@section('title')
Halaman Tampil Cast
@endsection

@section('content')
    <a href="{{ route("castCreate") }}" class="btn btn-primary btn-sm my-3">Tambah</a>
    <table class="table table-bordered">
    <thead>
        <tr>
            <th style="width: 10px">#</th>
            <th>Name</th>
            <th style="width: 250px">Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
        <tr>
            <td scope="row">{{ $key+1 }}</td>
            <td>{{$item->nama}}</td>
            <td>
                <form action="{{ route("castDestroy", ['id' => $item->id]) }}" method="post">
                    @method('DELETE')
                    @csrf
                    <a href="{{ route("castShow", ['id' => $item->id]) }}" class="btn btn-info btn-sm">Detail</a>
                    <a href="{{ route("castEdit", ['id' => $item->id]) }}" class="btn btn-warning btn-sm">Edit</a>
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                </form>
            </td>

        </tr>
        @empty
        <h1> Data Cast Kosong</h1>
        @endforelse
    </tbody>
</table>
@endsection
