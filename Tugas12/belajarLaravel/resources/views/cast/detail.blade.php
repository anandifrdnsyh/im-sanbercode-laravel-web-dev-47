@extends('layouts.master')

@section('title')
Halaman Detail Cast
@endsection

@section('content')
<h1>{{ $cast->nama }}</h1>
<p>{{ $cast->umur }}</p>
<span>{{ $cast->bio }}</span>
@endsection
