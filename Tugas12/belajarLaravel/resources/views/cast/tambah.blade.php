@extends('layouts.master')

@section('title')
Halaman Tambah Cast
@endsection

@section('content')
<form action="{{ route("castStore") }}" method="POST">
    @csrf
    <div class="form-group">
        <label>Nama Cast</label>
        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror">
    </div>
    @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Umur </label>
        <input type="number" name="umur" min="10" max="90" class="form-control @error('umur') is-invalid @enderror">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Bio</label>
        <textarea name="bio" class="form-control" cols="30" rows="10 @error('bio') is-invalid @enderror"></textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary float-right">Submit</button>
</form>
@endsection
