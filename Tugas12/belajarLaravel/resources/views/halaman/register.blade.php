@extends('layouts.master')

@section('title')
Register
@endsection

@section('content')
<form action="{{ route("welcome") }}" method="POST">
    @csrf
    <h1>Sign Up Form</h1>
    <label for="fristName">Frist name:</label> <br />
    <input type="text" name="fname" placeholder="Frist Name" /> <br /><br />
    <label for="lastName">Last name:</label> <br />
    <input type="text" name="lname" placeholder="Last Name" /><br /><br />

    <label for="gender">Gender</label> <br />
    <input type="radio" name="male" /> Male <br />
    <input type="radio" name="female" /> Female <br />
    <input type="radio" name="other" /> Other <br />
    <br />
    <label for="national">Nationality</label> <br />
    <select name="national">
        <option value="indonesia">Indonesia</option>
        <option value="Malaysia">Malaysia</option>
        <option value="Thailand">Thailand</option>
    </select>
    <br /><br />
    <label for="language">language Spoken:</label> <br />
    <input type="checkbox" name="bindonesia" />Bahasa Indonesia <br />
    <input type="checkbox" name="english" />English <br />
    <input type="checkbox" name="other" />Other <br />
    <br />
    <label for="bio">Bio:</label> <br />
    <textarea name="bio" cols="30" rows="10"></textarea> <br />

    <input type="submit" value="kirim" />
</form>
@endsection