<?php
require_once 'Animal.php';
require_once 'Frog.php';
require_once 'Ape.php';

$animal = new Animal("Shaun");
echo "Nama Hewan: " . $animal->getName() . "<br>";
echo "Legs: " . $animal->getLegs() . "<br>";
echo "Cold Bloded: " . $animal->getColdBlooded() . "<br>";
echo "<br>";

$frog = new Frog("Buduk");
echo "Nama Hewan: ". $frog->getName()."<br>"; // Output: Froggy
echo "Legs: ".$frog->getLegs()."<br>"; // Output: 4
echo "Cold Bloded: ".$frog->getColdBlooded()."<br>"; // Output: no
$frog->jump(); // Output: hop hop
echo "<br>";

$ape = new Ape("Kera Sakti");
echo "Nama Hewan: ". $ape->getName()."<br>"; // Output: Froggy
echo "Legs: ".$ape->getLegs()."<br>"; // Output: 4
echo "Cold Bloded: ".$ape->getColdBlooded()."<br>"; // Output: no
$ape->yell(); // Output: Auooo


?>