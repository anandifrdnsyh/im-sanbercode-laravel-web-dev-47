<?php
class Animal {
    private $name;
    public $legs= 4 ;
    private $cold_blooded;

    public function __construct($name) {
        $this->name = $name;
        $this->legs;
        $this->cold_blooded = "no";
    }

    public function getName() {
        return $this->name;
    }

    public function getLegs() {
        return $this->legs;
    }

    public function getColdBlooded() {
        return $this->cold_blooded;
    }

}


?>